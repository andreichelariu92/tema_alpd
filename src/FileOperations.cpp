#include "FileOperations.h"

#include <algorithm>
#include <iterator>
#include <fstream>
#include <cstring>
#include <iostream>
#include <cstdlib>

#include <dirent.h>

std::vector<std::string> getFileNames(std::string directory)
{
    DIR* d;
    struct dirent* dir;
    std::vector<std::string> fileNames;
    d = opendir(directory.c_str());

    if (d)
    {
        while ((dir = readdir(d)) != 0)
        {
            if (dir->d_type == DT_REG)
            {
                std::string fileName(dir->d_name);
                //avoid special files,
                //that have . as their first character
                if (fileName[0] != '.')
                {
                    //add to the filename the name of the
                    //directory
                    fileName = directory + "/" + fileName;
                    fileNames.push_back(fileName);
                }
            }
            else if (dir->d_type == DT_DIR)
            {
                std::string childDirectory(dir->d_name);

                //avoid searching for files in the
                //current directory or in the parent one
                if (childDirectory != "." 
                    && childDirectory != "..")
                {
                    std::vector<std::string> files = 
                    getFileNames(directory + "/" + childDirectory);

                    std::copy(files.begin(), files.end(),
                              std::back_inserter(fileNames));
                }
            }
        }
        closedir(d);
    }
    return fileNames;
}

void writeMapToFile(const std::map<std::string, int>& m,
                    std::string fileName)
{
    using std::map;
    using std::ofstream;
    using std::string;

    ofstream f(fileName.c_str(), std::ofstream::app);
    map<string, int>::const_iterator it;
    
    for (it = m.begin(); it != m.end(); ++it)
    {
        f<<(*it).first<<" ";
        f<<(*it).second<<std::endl;
    }

    f.flush();
}

std::map<std::string, int> readMapFromFile(std::string fileName)
{
    //INSPIRATION:http://stackoverflow.com/questions/20372661/read-word-by-word-from-file-in-c 
    
    std::map<std::string, int> result;
    std::ifstream f(fileName.c_str(), std::ifstream::in);
    std::string word;
    int number;

    while (f >> word >> number)
    {
        result.insert(make_pair(word, number));
    }
    return result;
}

void makeMapFromFile(std::string fileName,
                     std::map<std::string, int>& m)
{
    std::ifstream f(fileName.c_str(), std::ifstream::in);
    std::string word;

    while (f >> word)
    {
        if (m.find(word) == m.end())
            m.insert(make_pair(word, 1));
        else
            m[word] += 1;
    }
}

std::map<std::string, int> reduceMaps(const std::vector<std::map<std::string, int> >& v, char firstCharacter)
{
    using std::map;
    using std::vector;
    using std::string;

    map<string, int> result;
    vector<map<string, int> >::const_iterator vectorIt;
    map<string, int>::const_iterator mapIt;

    for (vectorIt = v.begin(); vectorIt != v.end(); ++vectorIt)
    {
        for (mapIt = (*vectorIt).begin();
             mapIt != (*vectorIt).end();
             ++mapIt)
        {
            string word = (*mapIt).first;
            int number = (*mapIt).second;

            if (word[0] == firstCharacter)
            {
                if (result.find(word) == result.end())
                    result.insert(make_pair(word, number));
                else
                    result[word] += number;
            }
            //exit the loop iterating the map
            //when we find the first word with
            //the first character over the searched one
            //i.e. if the first character is 'c',
            //we exit when we encounter a word starting with
            //letter 'd'
            else if (word[0] > firstCharacter)
                break;
        }
   }
   return result;
}
