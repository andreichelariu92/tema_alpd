#ifndef FileOperations_INCLUDE_GUARD
#define FileOperations_INCLUDE_GUARD

///FileOperations.h
///Contains the declaration of functions that operate on files.
#include <vector>
#include <string>
#include <map>

///Function that returns all the filenames from the
///directory given as argument
std::vector<std::string> getFileNames(std::string directory);

///Function that writes a map to a file.
void writeMapToFile(const std::map<std::string, int>& m,
                    std::string fileName);

///Function that reads a map from a file
std::map<std::string, int> readMapFromFile(std::string fileName);

///Function that makes a map from the contents
///of the file.
///The word,count pairs are added to the map
///given as parameter.
void makeMapFromFile(std::string fileName,
                     std::map<std::string, int>& m);

///Makes a map with words starting with firstCharacter
///from all the words present in the vector of maps.
std::map<std::string, int> reduceMaps(const std::vector<std::map<std::string, int> >& v, char firstCharacter);
#endif
