#include "Logger.h"

#include <mpi.h>
#include <time.h>
#include <stdio.h>

std::string getTime()
{
    //get the time structure
    time_t rawTime = time(0);
    struct tm* timeStructure = localtime(&rawTime);
    char* timeString = asctime(timeStructure);

    return std::string(timeString, 11, 8);
}

int Logger::m_mpiRank = 0;

void Logger::setMpiRank(int rank)
{
    Logger::m_mpiRank = rank;
}
LoggerLevel Logger::m_globalLoggerLevel = LL_ERROR;

void Logger::setGlobalLoggerLevel(LoggerLevel ll)
{
    Logger::m_globalLoggerLevel = ll;
}

Logger::Logger(LoggerLevel localLoggerLevel)
    :m_stringStream(),
     m_localLoggerLevel(localLoggerLevel)
{
}

std::stringstream& Logger::get()
{
    //add meta information
    m_stringStream<<"rank = "<<m_mpiRank<<"  "<<getTime()<<"  ";
    m_stringStream<<getLoggerLevelString(m_localLoggerLevel)<<"   ";

    return m_stringStream;
}

Logger::~Logger()
{
    if (m_localLoggerLevel <= m_globalLoggerLevel)
    {
        fprintf(stdout, m_stringStream.str().c_str());
        fflush(stdout);
    }
}

Logger::Logger(const Logger& source)
{
}

Logger& Logger::operator=(const Logger& source)
{
    return *this;
}

std::string getLoggerLevelString(LoggerLevel ll)
{
	std::string output;
	switch (ll)
	{
		case LL_ERROR:
			output = "E";
			break;
		case LL_INFO:
			output = "I";
			break;
		case LL_DEBUG:
			output = "D";
			break;
		default:
			break;	
	}

	return output;
}
