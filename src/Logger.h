#ifndef Logger_INCLUDE_GUARD_
#define Logger_INCLUDE_GUARD_

//INSPIRATION:
//http://www.drdobbs.com/cpp/logging-in-c/201804215?pgno=1 

///Logger.h
///File containing the declaration of the Logger class.

#include <sstream>
#include <string>

///Enum specifing the levels of logging
enum LoggerLevel
{
    ///only error messages are shown
    LL_ERROR = 0,
    ///errors and usefull info are shown
    LL_INFO = 1,
    ///errors, usefull info and debug messages
    ///are shown
    LL_DEBUG = 2
};
class Logger
{
public:
    ///constructor
    Logger(LoggerLevel localLoggerLevel);
    ///get the underlaying stringstream
    std::stringstream& get();
    ///set the LoggerLevel
    static void setGlobalLoggerLevel(LoggerLevel ll);
    ///set mpiRank
    static void setMpiRank(int rank);
    ///destructor
    ///the data written to the logger will
    ///be flushed to the stdout
    ~Logger();
private:
    ///stringstream containing all the date
    ///written to the logger in one scope
    std::stringstream m_stringStream;
    ///the rank of the process
    static int m_mpiRank;
    ///global logger level
    static LoggerLevel m_globalLoggerLevel;
    ///local logger level
    LoggerLevel m_localLoggerLevel;
    ///inhibit copy operations
    Logger(const Logger& source);
    Logger& operator=(const Logger& source);
};

///get the current time
std::string getTime();
///get the string for the corresponding
///logger level
std::string getLoggerLevelString(LoggerLevel ll);
#endif
