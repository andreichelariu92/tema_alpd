#include "Master.h"
#include "Logger.h"

Master::Master(MpiWrapper& mpi)
    : m_mpi(mpi),
      m_maps()
{
}

Master::Master(const Master& source)
    : m_mpi(source.m_mpi),
      m_maps(source.m_maps)
{
}

Master& Master::operator=(const Master& source)
{
    return *this;
}

void Master::sendFilesToSlaves(const std::vector<std::string>& files)
{
    using std::vector;
    using std::string;

    vector<string>::const_iterator it;
    Logger l(LL_INFO);

    for (it = files.begin(); it != files.end(); ++it)
    {
        //block the current thread untill
        //there is a slave available
        int slaveRank = m_mpi.waitAny();
        l.get() << "Talking to "<< slaveRank << '\n';
        //inform the slave that it has work to do
        m_mpi.sendMessage(MpiWrapper::MM_DO_WORK, slaveRank);
        //send him the filename to work on
        sendString((*it), slaveRank);
        l.get() << "Sending file: " << (*it) << '\n';
    }

    //notify all processes that the work is finished
    for (int procId = 1; procId < m_mpi.nrProcesses(); ++procId)
    {
        m_mpi.wait(procId);
        m_mpi.sendMessage(MpiWrapper::MM_STOP_WORK, procId);
    }

    //TO DO:
    //error handling
    {
        Logger l(LL_DEBUG);
        l.get() << "Finish sendFilesToSlaves\n";
    }
}

void Master::receiveMapsFromSlaves()
{
    using std::vector;
    using std::string;
    using std::map;

    int procId;
    const int nrProcesses = m_mpi.nrProcesses();

    for (procId = 1; procId < nrProcesses; ++procId)
    {
       Logger l(LL_INFO);
       l.get() << "Receive map from " << procId << '\n';
       m_maps.push_back(m_mpi.receiveWordMap(procId)); 
    }
    
    //for debugging
    /*
    vector<map<string, int> >::iterator vectorIt;
    map<string, int>::const_iterator mapIt;

    for (vectorIt = m_maps.begin();
         vectorIt != m_maps.end();
         ++vectorIt)
    {
        for (mapIt = (*vectorIt).begin();
             mapIt != (*vectorIt).end();
             ++mapIt)
        {
            l.get() << "Received " << (*mapIt).first
                    << " " << (*mapIt).second << '\n';
       }
        l.get() << '\n';
    }*/
    //TO DO:
    //error handling
}

void Master::sendMapsToSlaves()
{
    Logger l(LL_INFO);
    const int nrProcesses = m_mpi.nrProcesses();
    for (int procId = 1; procId < nrProcesses; ++procId)
    {
        l.get() << "send vector of maps to: " << procId << '\n';
        m_mpi.sendVectorOfWordMaps(m_maps, procId);
    }
    //TO DO:
    //error handling
}

void Master::reduceMapsByLetters()
{
    char letter;
    //for every letter wait a process
    //and tell him to reduce the maps
    //for that letter
    Logger l(LL_INFO);
    for (letter = 'A'; letter <= 'z'; ++letter)
    {
        int processId = m_mpi.waitAny();
        m_mpi.sendMessage(MpiWrapper::MM_DO_WORK,  processId);
        //send a string containing the letter
        //for wich the maps should be reduced.
        sendString(std::string(1, letter), processId);
        l.get() << "Process " << processId
                << " will reduce the maps for letter "
                << letter << '\n';
    }
    //tell all slaves that the work has been finished
    const int nrProcesses = m_mpi.nrProcesses();
    for (int procId = 1; procId < nrProcesses; ++procId)
    {
        m_mpi.wait(procId);
        m_mpi.sendMessage(MpiWrapper::MM_STOP_WORK, procId);
    }
}

std::vector<std::map<std::string, int> > Master::reduceFinalResult()
{
    //for all letters
    //-send all slaves a message to continue work
    //-send all slaves the letter to be received
    //-wait for the process which has that letter
    //-receive the map from him
    //send all slaves stop message

    using std::string;
    using std::map;
    using std::vector;
    
    vector<map<string, int> > result;
    Logger l(LL_INFO);
    for (char letter = 'A'; letter <= 'z'; ++letter)
    {
        m_mpi.broadcastMessage(MpiWrapper::MM_DO_WORK);
        broadcastString(std::string(1, letter));
        int procId = m_mpi.waitAny();
        map<string, int> reducedMap = m_mpi.receiveWordMap(procId);
        l.get() << "Received map for letter "
                << letter << " from "
                << procId << '\n';
        result.push_back(reducedMap);
    }

    m_mpi.broadcastMessage(MpiWrapper::MM_STOP_WORK);
    return result;
}
