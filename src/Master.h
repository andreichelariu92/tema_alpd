#ifndef Master_INCLUDE_GUARD
#define Master_INCLUDE_GUARD

///Master.h
///File containing the code that the master process
///will execute.
///1)Get the file names from the directory. DONE
///2)Send the file names to the slave processes. DONE
///3)Wait for all the slaves to finish and read
///the maps they produce. DONE
///4)Send the maps to all processes.
///5)Give each process a letter to reduce all words
///in the maps that start with that letter.
///6)After all letters have been finished, collect
///all the data and write it to a file.

#include <vector>
#include <string>

#include "MpiWrapper.h"

class Master
{
public:
    ///constructor
    Master(MpiWrapper& mpi);
    ///Sends the file names to the processes
    ///available. Each file in the vector is
    ///given to the first process that notifies
    ///the Master that it is ready.
    void sendFilesToSlaves(const std::vector<std::string>& files);
    ///Receive the maps from the slaves
    void receiveMapsFromSlaves();
    ///Send the maps received to all slaves
    void sendMapsToSlaves();
    ///Give each slave a letter to reduce
    ///the maps on.
    ///This process repeats untill all the letters
    ///in the A-z interval are reduced.
    void reduceMapsByLetters();
    ///Receives all the maps reduced by the slaves
    ///reduces them in the final form.
    std::vector<std::map<std::string, int> > reduceFinalResult();
    ///destructor
    ~Master(){}
private:
    ///reference to the mpiWrapper
    ///the Master class is not responsable
    ///for deleting it!
    MpiWrapper& m_mpi;
    ///vector of maps obtained from the slaves
    std::vector<std::map<std::string, int> > m_maps;
    ///inhibit copy operations
    Master(const Master& source);
    Master& operator=(const Master& source);
};
#endif
