///MpiWrapper.cpp
///File containing the definition of class
///MpiWrapper and some utility functions

#include "MpiWrapper.h"
#include "Logger.h"

using std::string;
using std::vector;
using std::map;
using std::pair;

MpiWrapper::MpiWrapper()
{
    MPI_Init(0, 0);
    MPI_Comm_size(MPI_COMM_WORLD, &m_nrProcesses);
    MPI_Comm_rank(MPI_COMM_WORLD, &m_processId);
}

MpiWrapper::MpiWrapper(const MpiWrapper& source)
    :m_processId(source.processId()),
     m_nrProcesses(source.nrProcesses())
{
    //warning message
    Logger l(LL_INFO);
    l.get()<<"Copy operations for MpiWrapper ";
    l.get()<<" are not permitted\n";
}

MpiWrapper& MpiWrapper::operator=(const MpiWrapper& source)
{
    m_processId = source.processId();
    m_nrProcesses = source.nrProcesses();

    //warning message
    Logger l(LL_INFO);
    l.get()<<"Copy operations for MpiWrapper ";
    l.get()<<" are not permitted\n";
 
    return *this;
}

MpiWrapper::~MpiWrapper()
{
    MPI_Finalize();
}


void MpiWrapper::sendVectorStrings(const vector<string>& v,
                                     int processId)
{
    unsigned int vectorSize = v.size();

    MPI_Send(&vectorSize, 1, MPI_INT, processId,
             0, MPI_COMM_WORLD);

    sendContainer(v.begin(), v.end(), sendString, processId);
    
    //TO DO:
    //error handling
}

vector<string> MpiWrapper::receiveVectorStrings(int processId)
{

    unsigned int vectorSize;

    MPI_Recv(&vectorSize, 1, MPI_INT, processId, 0,
             MPI_COMM_WORLD, 0);

    std::vector<std::string> v(vectorSize, "");

    receiveContainer(v.begin(), vectorSize,
                     receiveString, processId);

    Logger l(LL_DEBUG);
    l.get()<<m_processId<<"receiveVectorStrings: ";
    for (unsigned int vIdx = 0; vIdx < v.size(); ++vIdx)
    {
        
        l.get() << v[vIdx]<<'\n';
    }

    //TO DO:
    //error handling
    
    return v;
}

void MpiWrapper::sendWordMap(const map<string, int>& wordMap,
                             int processId)
{
    int mapSize = wordMap.size();
    MPI_Send(&mapSize, 1, MPI_INT, processId,
             0, MPI_COMM_WORLD);

    sendMap(wordMap, sendPair, processId);
}

map<string, int> MpiWrapper::receiveWordMap(int processId)
{
    int mapSize;
    map<string, int> result;
    MPI_Recv(&mapSize, 1, MPI_INT, processId,
             0, MPI_COMM_WORLD, 0);
    
    receiveMap(result, mapSize, receivePair, processId);
    
    //TO DO:
    //logger
    //error handling
    return result;
}

void MpiWrapper::sendVectorOfWordMaps
                 (const vector<map<string, int> >& v,
                  int processId)
{
    int vectorSize = v.size();
    
    MPI_Send(&vectorSize, 1, MPI_INT, processId,
             0, MPI_COMM_WORLD);

    SendWordMapWrapper sendWordMapWrapper(this);
    sendContainer(v.begin(),
                  v.end(),
                  sendWordMapWrapper,
                  processId);
}

vector<map<string, int> > MpiWrapper::receiveVectorOfWordMaps(int processId)
{
    unsigned int vectorSize;

    MPI_Recv(&vectorSize, 1, MPI_INT, processId, 0,
             MPI_COMM_WORLD, 0);
    vector<map<string, int> > result(vectorSize);
    
    ReceiveWordMapWrapper receiveWordMapWrapper(this);
    receiveContainer(result.begin(),
                     vectorSize,
                     receiveWordMapWrapper,
                     processId);
    return result;
}
int MpiWrapper::waitAny()
{
    char dummyData = 0;
    MPI_Status status;

    MPI_Recv(&dummyData, 1, MPI_CHAR,
             MPI_ANY_SOURCE, 0, MPI_COMM_WORLD,
             &status);

   return status.MPI_SOURCE; 

}

void MpiWrapper::wait(int processId)
{
    char dummyData = 0;
    MPI_Status status;

    MPI_Recv(&dummyData, 1, MPI_CHAR,
             processId, 0, MPI_COMM_WORLD,
             &status);

}

void MpiWrapper::notify(int processId)
{
    char dummyData = 0;
    MPI_Send(&dummyData, 1, MPI_CHAR,
             processId, 0, MPI_COMM_WORLD);
}
void MpiWrapper::sendMessage(MpiWrapper::MpiMessage m, int processId)
{
    MPI_Send(&m, 1, MPI_INT, processId, 0, MPI_COMM_WORLD);
}
MpiWrapper::MpiMessage MpiWrapper::receiveMessage(int processId)
{
    MpiWrapper::MpiMessage result;

    MPI_Recv(&result, 1, MPI_INT,
             processId, 0, MPI_COMM_WORLD,
             0);

    return result;
}
void MpiWrapper::broadcastMessage(MpiWrapper::MpiMessage message)
{
    MPI_Bcast(&message, 1, MPI_INT, m_processId, MPI_COMM_WORLD); 
}
MpiWrapper::MpiMessage MpiWrapper::receiveBroadcastMessage()
{
    MpiWrapper::MpiMessage result;
    MPI_Bcast(&result, 1, MPI_INT, 0, MPI_COMM_WORLD);
    return result;
}
void sendString(const string& message,
                int processId)
{
    //c_str() method returns a const char*
    //we convert that to a char* using
    //const_cast
    MPI_Send(const_cast<char*>(message.c_str()),
             message.size(), MPI_CHAR, processId,
             0, MPI_COMM_WORLD); 
    //TO DO:
    //logger
    //error handling
}
void broadcastString(const std::string& message)
{
    int processId;
    MPI_Comm_rank(MPI_COMM_WORLD, &processId);

    MPI_Bcast(const_cast<char*>(message.c_str()),
              message.size(),
              MPI_CHAR,
              processId,
              MPI_COMM_WORLD);
}
std::string receiveString(int processId)
{
    std::string message;
    char buffer[500];
    MPI_Status status;

    MPI_Recv(buffer, 500, MPI_CHAR, processId, 0,
             MPI_COMM_WORLD, &status);

    int count;
    MPI_Get_count(&status, MPI_CHAR, &count);
    if (count > 0)
    {
        message.assign(buffer, count);
    }

    //TO DO:
    //error handling
    return message;
}
std::string receiveBroadcastString()
{
    std::string message;
    char buffer[500];

    MPI_Bcast(buffer, 500, MPI_CHAR, 0, MPI_COMM_WORLD);

    message.assign(buffer);

    //TO DO:
    //error handling
    return message;
}
void sendPair(const pair<string, int>& p, int processId)
{
    sendString(p.first, processId);
    
    int i = p.second;
    MPI_Send(&i, 1, MPI_INT, processId, 0, MPI_COMM_WORLD);
}

pair<string, int> receivePair(int processId)
{
    pair<string, int> result;
    result.first = receiveString(processId);
    MPI_Recv(&result.second, 1, MPI_INT, processId,
             0, MPI_COMM_WORLD, 0);
    //TO DO:
    //error handling
    return result;
}
