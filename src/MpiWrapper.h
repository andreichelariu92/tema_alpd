#ifndef MpiWrapper_INCLUDE_GUARD
#define MpiWrapper_INCLUDE_GUARD

///MpiWrapper.h
///File containing the declaration of the MpiWrapper class
///and some utility functions.

#include <vector>
#include <map>
#include <string>
#include <utility>

#include <mpi.h>

class MpiWrapper
{
public:
    ///enum of messages used to
    ///synchronize the Slaves and the Master
    enum MpiMessage
    {
        MM_DO_WORK = 10,
        MM_STOP_WORK = 11
    };
    ///constructor
    MpiWrapper();
    ///returns the rank of the current process
    int processId() const
    {
        return m_processId;
    }
    ///returns the number of processes
    int nrProcesses() const
    {
        return m_nrProcesses;
    }
    ///sends a vector of strings
    ///to the process identified by processId
    void sendVectorStrings(const std::vector<std::string>& vector,
                            int processId);
    ///receives a vector of strings
    ///from the process identified
    ///by processId
    std::vector<std::string> receiveVectorStrings(int processId);
    ///sends a map of strings and words
    ///to the process identified by
    ///processId
    void sendWordMap(const std::map<std::string, int>& wordMap,
                     int processId);
    ///receives a map of strings and ints
    ///from the process identified by
    ///processId
    std::map<std::string, int> receiveWordMap(int processId);
    ///sends a vector of maps to the process
    ///with the specified id
    void sendVectorOfWordMaps(const std::vector<std::map<std::string, int> >& v, int processId);
    ///receives a vector of maps
    ///from the process with the specifed id
    std::vector<std::map<std::string, int> > receiveVectorOfWordMaps(int processId);
    ///waits for any process to notify
    ///the current one
    ///returns the rank of the process
    ///that called notify
    int waitAny();
    ///waits for the specified
    ///process to call notify
    void wait(int processId);
    ///notifies the specified process
    void notify(int processId);
    ///sends the message m to the
    ///process with the specified id
    void sendMessage(MpiWrapper::MpiMessage m, int processId);
    ///receives a message from the
    ///process with the specified id
    MpiWrapper::MpiMessage receiveMessage(int processId);
    ///broadcast message
    ///to all processe except the
    ///current one
    void broadcastMessage(MpiWrapper::MpiMessage m);
    ///receive a message that has been
    ///broadcasted by the root process
    MpiWrapper::MpiMessage receiveBroadcastMessage();
    ///destructor
    ~MpiWrapper();
private:
    ///inhibit copy operations
    MpiWrapper(const MpiWrapper& source);
    MpiWrapper& operator=(const MpiWrapper& source);
    ///id of the current process
    int m_processId;
    ///nr of processes
    int m_nrProcesses;

};

///Generic function that sends a container
///to a specified process.
///begin, end: a pair of iterators that point
///to the begining and ending of the container.
///sendFunction: function that sends an element
///of the container.
///destinationProcess: the rank of the process
///that will receive the container
template<typename Iterator,
         typename Functor>
void sendContainer(Iterator begin, Iterator end,
                   Functor sendFunction,
                   int destinationProcess)
{
    while (begin != end)
    {
        sendFunction(*begin, destinationProcess);
        ++begin;
    }
}

///Generic function that sends a map to a
///specified process.
///m: a const reference to a map<Key, Value>
///sendFunction: a function that will send a
///pair<Key, Value> to the destination
///destinationProcess: the rank of the process
///that will receive the map.
template<typename Functor,
         typename Key,
         typename Value>
void sendMap(const std::map<Key, Value>& m,
             Functor sendFunction,
             int destinationProcess)
{
    typename std::map<Key, Value>::const_iterator it;
    for (it = m.begin(); it != m.end(); ++it)
    {
        sendFunction(*it, destinationProcess);
    }
}

///Generic function that receives a container
///from a process.
///begin: an iterator that points at the begining
///of the container. The container must have enough
///memory to receive the data!
///numberOfData: number of items to be read.
///receiveFunction: function that will receive an
///element and it to the container.
///sourceProcess: the rank of the process that will
///send the data.
template<typename Iterator,
         typename Functor>
void receiveContainer(Iterator begin,
                      int numberOfData,
                      Functor receiveFunction,
                      int sourceProcess)
{
    while (numberOfData > 0)
    {
        *begin = receiveFunction(sourceProcess);
        ++begin;
        --numberOfData;
    }
}

///Generic function that receives a map from a
///specified process.
///m: a reference to a map<Key, Value>
///numberOfData: how many items to add to the map
///receiveFunction: function that receives
///a pair<Key, Value> from the source.
///sourceProcess: the rank of the process that will
///send the data.
template<typename Functor,
         typename Key,
         typename Value>
void receiveMap(std::map<Key,Value>& m,
                int numberOfData,
                Functor receiveFunction,
                int sourceProcess)
{
    while (numberOfData > 0)
    {
        std::pair<Key, Value> p = receiveFunction(sourceProcess);
        m.insert(p);
        --numberOfData;
    }
}

///function that sends a string to a process
///with the specified rank.
void sendString(const std::string& message, int processId);

///brodcasts the string
///to all the processes,
///except the current one
void broadcastString(const std::string& message);

///function that receives a string from a process
///with the specified rank.
std::string receiveString(int processId);

///function that receives a string that
///has been broadcasted
std::string receiveBroadcastString();

///function that sends a pair of string and int
///to a process with the specified rank
void sendPair(const std::pair<std::string, int>& p,
              int processId);

///function that receives a pair of string
///and int from a process with the specified
///rank
std::pair<std::string, int> receivePair(int processId);

///Dear unfortunate person,
///SendWordMapWrapper is a structure
///that has overloaded operator().
///This way it can be called like a function.
///The purpose of this is to be used with
///the generic algorithms presented above.
///The operator() is basically just a wrapper
///that calls sendWordMap for the ptr instance.
///In practice, ptr will be the constant pointer this.
struct SendWordMapWrapper
{
    MpiWrapper *const ptr;
    SendWordMapWrapper(MpiWrapper *const p)
        : ptr(p)
    {
    }
    void operator() (const std::map<std::string, int>& m,
                     int processId)
    {
        ptr->sendWordMap(m, processId);
    }
    
};

///Dear unfortunate person,
///ReceiveWordMapWrapper is a structure
///that has overloaded operator().
///This way it can be called like a function.
///The purpose of this is to be used with
///the generic algorithms presented above.
///The operator() is basically just a wrapper
///that calls receiveWordMap for the ptr instance.
///In practice, ptr will be the constant pointer this.
struct ReceiveWordMapWrapper
{
    MpiWrapper *const ptr;
    ReceiveWordMapWrapper(MpiWrapper *const p)
        :ptr(p)
    {
    }
    std::map<std::string, int> operator() (int processId)
    {
        return ptr->receiveWordMap(processId);
    }

};
#endif
