#include "Slave.h"
#include "FileOperations.h"
#include "Logger.h"

Slave::Slave(MpiWrapper& mpi)
    : m_map(),
     m_vectorMaps(),
     m_reducedMaps(),
     m_mpi(mpi) 
{
}

Slave::Slave(const Slave& source)
    :m_map(source.m_map),
     m_vectorMaps(source.m_vectorMaps),
     m_reducedMaps(source.m_reducedMaps),
     m_mpi(source.m_mpi)
      
{
}

Slave& Slave::operator=(const Slave& source)
{
    return *this;
}

Slave::~Slave()
{
}

void Slave::processFiles()
{
    MpiWrapper::MpiMessage message = MpiWrapper::MM_DO_WORK;
    Logger l(LL_DEBUG);
    while (message == MpiWrapper::MM_DO_WORK)
    {
        //notify the Master that I am available
        m_mpi.notify(0);
        message = m_mpi.receiveMessage(0);

        if (message == MpiWrapper::MM_DO_WORK)
        {
            std::string fileName = receiveString(0);
            l.get() << "Start processing " << fileName << '\n';
            makeMapFromFile(fileName, m_map);
            l.get() << "End processing " << fileName << '\n';
        }
    }

    //TO DO:
    //error handling
}

void Slave::sendMapToMaster()
{
    m_mpi.sendWordMap(m_map, 0);
}

void Slave::receiveMapsFromMaster()
{
    using std::vector;
    using std::string;
    using std::map;

    m_vectorMaps = m_mpi.receiveVectorOfWordMaps(0);
    /*
    Logger l(LL_DEBUG);
    vector<map<string, int> >::iterator vectorIt;
    map<string, int>::const_iterator mapIt;
    
    for (vectorIt = m_vectorMaps.begin();
         vectorIt != m_vectorMaps.end();
         ++vectorIt)
    {
        l.get() << "Received map: \n";
        for (mapIt = (*vectorIt).begin();
             mapIt != (*vectorIt).end();
             ++mapIt)
        {
            l.get() << (*mapIt).first
                    << " "
                    << (*mapIt).second
                    << '\n';
        }
    }
    */
}

void Slave::reduceMapsByLetters()
{
    using std::map;
    using std::string;

    MpiWrapper::MpiMessage message = MpiWrapper::MM_DO_WORK;
    Logger l(LL_DEBUG);
    while (message == MpiWrapper::MM_DO_WORK)
    {
        //tell the master that I am available
        m_mpi.notify(0);
        message = m_mpi.receiveMessage(0);
        
        if (message == MpiWrapper::MM_DO_WORK)
        {
            string letter = receiveString(0);
            map<string, int> reducedMap = reduceMaps(m_vectorMaps,
                                                     letter[0]);
            m_reducedMaps.insert(make_pair(letter[0], reducedMap));
        }
    }
    l.get() << "Exit function reduceMapsByLetters\n"; 
    map<char, map<string, int> >::const_iterator redMapIt;
    map<string, int>::const_iterator mapIt;
    /*
    for (redMapIt = m_reducedMaps.begin();
         redMapIt != m_reducedMaps.end();
         ++redMapIt)
    {
        l.get() << "For letter " << (*redMapIt).first << '\n';
        for (mapIt = (*redMapIt).second.begin();
             mapIt != (*redMapIt).second.end();
             ++mapIt)
        {
            l.get() << (*mapIt).first << " "
                    << (*mapIt).second << '\n';
        }
    }
    */
}

void Slave::sendReducedMapsToMaster()
{
    using std::string;
    using std::map;
    using std::vector;

    MpiWrapper::MpiMessage message = MpiWrapper::MM_DO_WORK;

    while (message == MpiWrapper::MM_DO_WORK)
    {
        
        message = m_mpi.receiveBroadcastMessage();
        
        if (message == MpiWrapper::MM_DO_WORK)
        {
            string letter = receiveBroadcastString();
            //if the current slave has the map
            //for the asked letter, send it to
            //the master
            if (m_reducedMaps.find(letter[0]) 
                != m_reducedMaps.end())
            {
                m_mpi.notify(0);
                m_mpi.sendWordMap(m_reducedMaps[letter[0]], 0);
            }
        }
    }
    Logger l(LL_DEBUG);
    l.get() << "Exit with success from sendReducedMapsToMaster\n";
}
