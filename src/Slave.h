#ifndef Slave_INCLUDE_GUARD
#define Slave_INCLUDE_GUARD

///File Slave.h
///Constains the definition of the Slave class
///Responsabilities of the Slave class:
///1)Receive files from the Master
///and make a map from them. DONE
///2)Send the map to the master. DONE
///3)Receive all the maps from the master. DONE
///4)Reduce the maps for the letters given by the
///master.

#include <vector>
#include <string>
#include <map>

#include "MpiWrapper.h"

class Slave
{
public:
    ///constructor
    Slave(MpiWrapper& mpi);
    ///receives fileNames from files
    ///and makes a map out of them
    void processFiles();
    std::map<std::string, int>& wordMap()
    {
        return m_map;
    }
    ///Sends the map formed after processing
    ///the files to the master
    void sendMapToMaster();
    ///receives all of the maps from the master
    void receiveMapsFromMaster();
    ///reduce the vector of maps for the letters
    ///received from the Master
    void reduceMapsByLetters();
    ///send the reduced maps
    ///to the Master, when he asks
    ///for them
    void sendReducedMapsToMaster();
    ~Slave();
private:
    Slave(const Slave& source);
    Slave& operator=(const Slave& source);
    std::map<std::string, int> m_map;
    std::vector<std::map<std::string, int> > m_vectorMaps;
    std::map<char, std::map<std::string, int> > m_reducedMaps;
    MpiWrapper& m_mpi;
};
#endif
