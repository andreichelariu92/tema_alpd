#include "UserInterface.h"

#include <vector>
#include <string>
#include <iostream>
#include <cstdlib>

void ProgramArguments::showHelpMessage()
{
    using std::cout;

    if (showHelp)
    {
        cout << "Usage: mpirun -np NR_PROC "
             << "mapReduce -d DIR_NAME "
             << "-f FILE_NAME "
             << "-v verbosity level\n";
        cout << "Argument semnifications: \n";
        cout << "-d specifies the directory to be processed."
             << "The name of the directory must not end with / \n";
        cout << "-f specifies the filename where the output "
             << "is stored\n";
        cout << "-v specifies the verbosity level:\n";
        cout << "    0 only error messages are shown\n";
        cout << "    1 errors and usefull information are shown\n";
        cout << "    2 errors, usefull info and debug information"
             << " are shown\n";
        cout << "Copyright (c) 2015-2016 Andrei Chelariu\n";
        cout << "Check LICENSE for more information.\n"; 
    }
}
ProgramArguments parseArguments(int argc, char** argv)
{
    //INSPIRATION:
    //http://stackoverflow.com/questions/441547/most-efficient-way-to-process-arguments-from-the-command-line-in-c 
    using std::vector;
    using std::string;

    ProgramArguments result;
    result.directoryName = ".";
    result.outputFileName = "output";
    result.loggerLevel = LL_ERROR;
    result.showHelp = false;

    if (argc == 1)
    {
        result.showHelp = true;
    }
    vector<string> args(argv + 1, argv + argc);
    vector<string>::iterator it;

    for (it = args.begin(); it != args.end(); ++it)
    {
        //with *(++it) I get the argument
        //folowing the current one
        //for example: -d dir
        //*it will be -d
        //and *(++it) will be dir
        if (*it == "-h")
        {
            result.showHelp = true;
        }
        else if (*it == "-d")
        {
            result.directoryName = *(++it);
        }
        else if (*it == "-f")
        {
            result.outputFileName = *(++it);
        }
        else if (*it == "-v")
        {
            string levelAsString = *(++it);
            int loggerLevel = atoi(levelAsString.c_str());
            switch (loggerLevel)
            {
                case 0:
                    result.loggerLevel = LL_ERROR;
                    break;
                case 1:
                    result.loggerLevel = LL_INFO;
                    break;
                case 2:
                    result.loggerLevel = LL_DEBUG;
                    break;
            }
        }
        else
        {
           result.showHelp = true;
        }
    }

    return result;
}
