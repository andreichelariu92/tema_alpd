#ifndef UserInterface_INCLUDE_GUARD
#define UserInterface_INCLUDE_GUARD

///File: UserInterface.h
///Contains the declaration of
///the ProgramArguments structure.
///This structure contains the arguments
///passed at the command line by the user,
///or the default ones.

#include <string>

#include "Logger.h"

struct ProgramArguments
{
    std::string directoryName;
    std::string outputFileName;
    LoggerLevel loggerLevel;
    bool showHelp;
    void showHelpMessage();
};

ProgramArguments parseArguments(int argc, char** argv);

#endif
