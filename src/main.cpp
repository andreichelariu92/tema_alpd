#include <iostream>

#include "Logger.h"
#include "FileOperations.h"
#include "Master.h"
#include "Slave.h"
#include "UserInterface.h"

int main(int argc, char** argv)
{
    ProgramArguments arguments = parseArguments(argc, argv);
    MpiWrapper mpi;
    Logger::setGlobalLoggerLevel(arguments.loggerLevel);
    Logger::setMpiRank(mpi.processId());

    int myId = mpi.processId();
    if (myId == 0)
    {
        using std::vector;
        using std::string;
        using std::map;

        //show the help message only from the root process
        //to prevent it being shown multiple times.
        arguments.showHelpMessage();

        Master master(mpi);
        vector<string> files = getFileNames(arguments.directoryName);
        master.sendFilesToSlaves(files);
        master.receiveMapsFromSlaves();
        master.sendMapsToSlaves();
        master.reduceMapsByLetters();
        
        vector<map<string, int> > v = master.reduceFinalResult();

        vector<map<string, int> >::iterator vIt;
        string outputFileName(arguments.outputFileName);
        for (vIt = v.begin(); vIt != v.end(); ++vIt)
        {
            writeMapToFile((*vIt), outputFileName);
        }
    }
    else
    {
        using std::map;
        using std::string;

        Slave slave(mpi);
        slave.processFiles();
        slave.sendMapToMaster();
        slave.receiveMapsFromMaster();
        slave.reduceMapsByLetters();
        slave.sendReducedMapsToMaster();
    }
    return 0;
}
