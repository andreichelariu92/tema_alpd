#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>

#include <dirent.h>

std::vector<std::string> getFileNames(std::string directory)
{
    DIR* d;
    struct dirent* dir;
    std::vector<std::string> fileNames;
    d = opendir(directory.c_str());

    if (d)
    {
        while ((dir = readdir(d)) != 0)
        {
            if (dir->d_type == DT_REG)
            {
                std::string fileName(dir->d_name);
                fileNames.push_back(fileName);
            }
            else if (dir->d_type == DT_DIR)
            {
                std::string directoryName(dir->d_name);

                if (directoryName != "." 
                    && directoryName != "..")
                {
                    std::vector<std::string> files = 
                    getFileNames(directory + "/" + directoryName);

                    std::copy(files.begin(), files.end(),
                              std::back_inserter(fileNames));
                }

            }
        }
        closedir(d);
    }
    return fileNames;
}
int main()
{
    using std::vector;
    using std::string;

    vector<string> files = getFileNames("./");
    vector<string>::iterator it;
    std::cout<<"Resulted files are: \n";
    for (it = files.begin(); it != files.end(); ++it)
        std::cout<<(*it)<<" ";
    std::cout << '\n';
    return 0;
}
