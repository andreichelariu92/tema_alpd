#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <map>

#include <mpi.h>

template<typename Iterator,
         typename Functor>
void sendContainer(Iterator begin, Iterator end,
                   Functor sendFunction,
                   int destinationProcess)
{
    while (begin != end)
    {
        sendFunction(*begin, destinationProcess);
        ++begin;
    }
}
template<typename Functor,
         typename Key,
         typename Value>
void sendMap(std::map<Key, Value>& m,
             Functor sendFunction,
             int destinationProcess)
{
    typename std::map<Key, Value>::iterator it;
    for (it = m.begin(); it != m.end(); ++it)
    {
        sendFunction(*it, destinationProcess);
    }
}
template<typename Iterator,
         typename Functor>
void receiveContainer(Iterator begin,
                      int numberOfData,
                      Functor receiveFunction,
                      int sourceProcess)
{
    while (numberOfData > 0)
    {
        *begin = receiveFunction(sourceProcess);
        ++begin;
        --numberOfData;
    }
}
template<typename Functor,
         typename Key,
         typename Value>
void receiveMap(std::map<Key,Value>& m,
                int numberOfData,
                Functor receiveFunction,
                int sourceProcess)
{
    while (numberOfData > 0)
    {
        std::pair<Key, Value> p = receiveFunction(sourceProcess);
        m.insert(p);
        --numberOfData;
    }
}

int receiveInt(int sourceProcess)
{
    int result;
    MPI_Recv(&result, 1, MPI_INT, sourceProcess,
             0, MPI_COMM_WORLD, 0);
    
    return result;
}
void sendInt(int nr, int destinationProcess)
{
    MPI_Send(&nr, 1, MPI_INT, destinationProcess,
             0, MPI_COMM_WORLD); 
}

std::pair<char, int> receivePair(int sourceProcess)
{
    std::pair<char, int> result;
    char c;
    int i;
    MPI_Recv(&c, 1, MPI_CHAR, sourceProcess,
             0, MPI_COMM_WORLD, 0);

    MPI_Recv(&i, 1, MPI_INT, sourceProcess,
             0, MPI_COMM_WORLD, 0);

    result.first = c;
    result.second = i;
    return result;
}

void sendPair(std::pair<char, int> p, int destinationProcess)
{
    int i = p.second;
    char c = p.first;

    MPI_Send(&c, 1, MPI_CHAR, destinationProcess,
             0, MPI_COMM_WORLD);

    MPI_Send(&i, 1, MPI_INT, destinationProcess,
             0, MPI_COMM_WORLD);
}

int main(int argc, char** argv)
{
    MPI_Init(0, 0);

    

    int nrProcesses;
    MPI_Comm_rank(MPI_COMM_WORLD, &nrProcesses);

    int idProcess;
    MPI_Comm_rank(MPI_COMM_WORLD, &idProcess);

    if (idProcess == 0)
    {
        std::vector<int> v;
        v.push_back(10);
        v.push_back(100);
        //send
        sendContainer(v.begin(), v.end(), sendInt, 1);
    }
    else
    {
        //receive
        std::vector<int> v(2, 0);
        receiveContainer(v.begin(), 2, receiveInt, 0); 
        std::cout<<"received \n";

        std::cout<<v.size()<<"\n";

        std::vector<int>::iterator it;
        for (it = v.begin(); it != v.end(); ++it)
        {
            std::cout<<(*it)<<" ";
        }
           

    }

    MPI_Finalize();
}
